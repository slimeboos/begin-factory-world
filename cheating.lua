local stolen = require("stolen")

local function reset(_, _)
    game.print("Resetting")
    global["oilmode"] = nil
    global["firemode"] = nil
end

local function create(player, args)
    local potentialEntity = args[2]
    player.surface.create_entity({
      name     = potentialEntity,
      position = player.position,
  })
end

local function firemode(_, _)
    game.print("FIRE MODE TIME")
    global["firemode"] = true
end

local function create_biters(player, _)
    player.surface.create_entity{
      name = "behemoth-biter",
      position = {
        player.position.x + 15,
        player.position.y
      }
    }
end

local function killmode(_, _)
    game.print("KILL MODE TIME")
    global["killmode"] = true
end

local function oilmode(_, _)
    game.print("OIL MODE TIME")
    global["oilmode"] = true
end

local function launch(player, _)
    stolen.launch(player, player.position)
end

local function kill(_, _)
    game.forces['enemy'].kill_all_units()
end

local function friend(player, _)
    game.surfaces[1].create_entity{
      name     = "big-biter",
      position = {player.position.x + 10, player.position.y + 10},
      force    = game.forces.player,
    }
end

local function killall(entity_type)
  for _, entity in ipairs(
    game.player.surface.find_entities_filtered{
      area={
        { game.player.position.x-1000, game.player.position.y-1000 },
        { game.player.position.x+1000, game.player.position.y+1000 }
      },
        type=entity_type,
      }
    ) do
    entity.destroy()
  end
end

return {
  create        = create,
  create_biters = create_biters,
  firemode      = firemode,
  friend        = friend,
  kill          = kill,
  killall       = killall,
  killmode      = killmode,
  launch        = launch,
  oilmode       = oilmode,
  reset         = reset,
}
