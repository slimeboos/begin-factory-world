local direction     = require("direction")
local stolen        = require("stolen")
local notifications = require("notifications")

local Slots = {
  ["coal"]       = defines.inventory.chest,
  ["iron-ore"]   = defines.inventory.chest,
  ["iron-plate"] = defines.inventory.furnace_result,

  -- inventory.fuel
  -- inventory.chest
  -- inventory.furnace_source
  -- inventory.furnace_result
  -- inventory.furnace_modules
  -- inventory.player_main
  -- inventory.player_quickbar
  -- inventory.player_guns
  -- inventory.player_ammo
  -- inventory.player_armor
  -- inventory.player_tools
  -- inventory.player_vehicle
  -- inventory.player_trash
  -- inventory.assembling_machine_input
  -- inventory.assembling_machine_output
  -- inventory.assembling_machine_modules
  -- inventory.lab_input
  -- inventory.lab_modules
}

local function mine(player, _)
    global["minemode"] = true
    player.update_selected_entity(player.position)
    player.mining_state = {mining = true, position = player.position}
end

local function stopmine(player, _)
    player.walking_state = {walking = false, direction = defines.direction.north}
    global["minemode"] = nil
end

-- Example:
--   $build stone-furnace
local function build_entity(player, args)
    local entity  = args[1]
    local pos     = direction.right_in_front_of_player(player)
    local success = stolen.build(player, pos, entity, player.character.direction)
    if not success then
      return "Unsuccessful build of " .. entity
    end
end

-- This probably wants all
-- https://lua-api.factorio.com/0.14.7/LuaControl.html#LuaControl.get_craftable_count
local function craft_entity(player, args)
    local entity          = args[1]
    local craftable_count = player.get_craftable_count(entity)
    local amount          = 1
    if craftable_count >= amount then
      stolen.craft(player, amount, entity)
    else
      local msg = string.format("Not Enough Ingredients to Craft %s", entity)
      notifications.notify(msg)
    end
end

-- We could find the specific entity, if we know the name
-- https://lua-api.factorio.com/0.12.35/LuaSurface.html#find_entity
local function destroy(player, _)
  local area = direction.box_in_front_of_player(player)

  local right_bottom = area[1]
  local left_top = area[#area]
  local box = {
    right_bottom = right_bottom,
    left_top = left_top,
  }

  rendering.draw_rectangle{
    surface      = player.surface,
    right_bottom = right_bottom,
    left_top     = left_top,

    -- How should I make this random
    -- Or should I color this based on something useful
    color        = {1, 1, 3},
    width        = 2,
    time_to_live = 300,
  }

  local entities = game.surfaces["nauvis"].find_entities(box)

  for _, entity in ipairs(entities) do
    game.print(entity.name)
    local success = player.mine_entity(entity)
    if not success then
        notifications.notify(string.format("Failed to Destroy %s" ,entity.name))
    end
  end
end

local function rotate(player, _)
  local pos = direction.right_in_front_of_player(player)
  local farPos = direction.right_in_front_of_player(player, 2)

  local entities = game.surfaces["nauvis"].find_entities({pos, farPos})
  for _, entity in ipairs(entities) do
    print(entity.name)
    if entity.rotatable then
      local success = entity.rotate()
      if not success then
        notifications.notify(string.format("Failed to Rotate %s" ,entity.Name))
      end
    else
      notifications.notify(string.format("%s Not Rotatable" ,entity.Name))
    end
  end
end


-- inventory.chest
-- $take
--
-- Things to Explore
-- entity.get_output_inventory()
-- inventory.get_item_count()
-- inventory.remove({name=itemName, count=amount})
local function take(player, args)
  local itemName = args[1]
  -- Thing of a sensible default
  -- or set to the last thing taken
  -- and take again
  if itemName == nil then
    itemName = "iron-plate"
  end

  local xStart = -20
  local yStart = -20
  local xEnd   = 20
  local yEnd   = 20
  local amount = 100
  local slot   = Slots[itemName]

  local entities = game.get_surface(1).find_entities_filtered{
    area = {
      {player.position.x + xStart, player.position.y + yStart},
      {player.position.x + xEnd,   player.position.y + yEnd}
    },
    name = {"stone-furnace","iron-chest", "wooden-chest"},
  }

  for _, entity in ipairs(entities) do
    print(entity.name)
    stolen.take(player, {entity.position.x, entity.position.y}, itemName, amount, slot)
  end
end

return {
  build_entity = build_entity,
  craft_entity = craft_entity,
  destroy      = destroy,
  mine         = mine,
  rotate       = rotate,
  stopmine     = stopmine,
  take         = take,
}
