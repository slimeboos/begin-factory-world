
-- ~/.factorio/script-output/notifications.txt
local function notify(message)
  game.write_file("notifications.txt", serpent.block(message))
end

-- TODO: Move this somewhere else
local function flying_text(player, msg)
   player.create_local_flying_text(
   {
     text         = msg,
     position     = { player.position.x, player.position.y },
     time_to_live = 500,
     color        = { r = 1, g = 0.5, b = 1, a = 1 }
   })
end


return {
  notify = notify,
  flying_text = flying_text,
}
