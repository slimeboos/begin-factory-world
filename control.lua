--control.lua

require ('util')

local console_chat  = require("console_chat")
local character     = require("character")
local notifications = require("notifications")


script.on_init(function() -- STARTING THE MOD
  game.print("Starting Script!")
  global["firemode"] = nil
  global["oilmode"]  = nil
  global["killmode"] = nil
end)


script.on_event(defines.events.on_console_chat, -- EVERY TIME WE GET CONSOLE CHAT
  function(event)

    local success, message = pcall(console_chat.handle, event)
    if message ~= nil then
      print("Message: " .. message)
      notifications.notify(message)
    end

    if success then
      print("Success")
    else
      game.print(message)
      print("Failure")
    end
end)


script.on_event(defines.events.on_player_changed_position, -- EVERY TIME THE PLAYER MOVES
  function(event)
    local player = game.get_player(2) -- get the player that moved

    if player ~= nil then
      pcall(character.maxhealth, player)
      if global["firemode"] == true then
        player.surface.create_entity{name="fire-flame", position=player.position, force="neutral"}
      end

      if global["oilmode"] == true then
        player.surface.create_entity({name='crude-oil', amount=100000, position={player.position.x, player.position.y}})
      end
    end

    if global["killmode"] == true then
      game.forces["enemy"].kill_all_units()
    end

  end)


script.on_event(defines.events.on_tick, function(event) -- EVERY TICK
  local player = game.get_player(2)

  if player ~= nil then
      pcall(character.maxhealth, player)

      if global["minemode"] == true then
          if player ~= nil then
              player.update_selected_entity(player.position)
              player.mining_state = {mining = true, position = player.position}
          end
      end

      if global["player.walking"] ~= nil then
        Pos = global["player.walking"]

        -- TODO: This needs to take into account the players direction they are going
        if Pos.x ~= nil and Pos.y ~= nil then
          -- What if we overshoot the walking??
          if (math.abs(player.position.x - Pos.x) < 0.5) and (math.abs(player.position.y - Pos.y) < 0.5) then
            global["player.walking"] = nil
            player.walking_state = {walking = false, direction = defines.direction.north}
          end
        end
      end

    end

end)


script.on_event(defines.events.on_player_cancelled_crafting, function(event)
    local player = game.get_player(2)

    if player ~= nil then
      local msg = string.format("Cancelled %s", event.recipe.name)
      notifications.flying_text(player, msg)
   end
end)


script.on_event(defines.events.on_pre_player_crafted_item, function(event)
    local player = game.get_player(2)

    if player ~= nil then
      local msg = string.format("Pre Craft %s", event.recipe.name)
      notifications.flying_text(player, msg)
   end
end)


script.on_event(defines.events.on_player_crafted_item, function(event)
    local player = game.get_player(2)

    if player ~= nil then
      local msg = string.format("Crafted %s", event.recipe.name)
      notifications.flying_text(player, msg)
      notifications.notify(msg)
   end
end)


script.on_nth_tick(900, -- every 15 seconds
  function(_)
    local player = game.get_player(2)
    if player ~= nil then
      local msg = string.format("X %d Y %d", player.position.x, player.position.y)
      notifications.flying_text(player, msg)
    end
  end)


script.on_event(defines.events.on_console_command, -- EVERY TIME WE CONSOLE COMMANDS
  function(_)
end)
