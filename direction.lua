require ("util")

local stolen = require("stolen")

local dir_modifiers = {
  [defines.direction.north]     = {0, -1},
  [defines.direction.south]     = {0, 1},
  [defines.direction.east]      = {1, 0},
  [defines.direction.west]      = {-1, 0},
  [defines.direction.northeast] = {1, -1},
  [defines.direction.northwest] = {-1, -1},
  [defines.direction.southeast] = {1, 1},
  [defines.direction.southwest] = {-1, 1},
}

local function stop(player, _)
    player.walking_state = {walking = false, direction = defines.direction.north}
    -- return "Stopping NOW!"
end

local function stop_everything(player, _)
    player.walking_state = {walking = false, direction = defines.direction.north}
    global["minemode"] = false
end

local function stop_everything(player, _)
    player.walking_state = {walking = false, direction = defines.direction.north}
    global["minemode"] = false
end


local function move_in_dir(player, dir, args)
  player.mining_state = {mining = false, position = player.position}
  global["minemode"] = false

  player.walking_state = {walking = true, direction = dir}
  local rawModValue = args[1]
  if rawModValue == nil then return end

  global["player.walking"] = nil
  local modVal = tonumber(rawModValue)
  local mods = dir_modifiers[dir]
  local xMod = mods[1]
  local yMod = mods[2]
  local x = player.position.x + (modVal * xMod)
  local y = player.position.y + (modVal * yMod)

  global["player.walking"] = { ["x"] = x, ["y"] = y }
end

local function dir(player, args)
    global["player.walking"] = nil
    local rawX = args[1]
    local rawY = args[2]
    local x = tonumber(rawX)
    local y = tonumber(rawY)

    player.walking_state = stolen.walk(x,y)
    local newPos = {
      ["x"] = (player.position.x + x),
      ["y"] = (player.position.y + y),
    }
    global["player.walking"] = newPos
end


local function move_east(player, args)
  move_in_dir(player, defines.direction.east, args)
end

local function move_west(player, args)
  move_in_dir(player, defines.direction.west, args)
end

local function move_south(player, args)
  move_in_dir(player, defines.direction.south, args)
end

local function move_north(player, args)
  move_in_dir(player, defines.direction.north, args)
end

local function move_southeast(player, args)
  move_in_dir(player, defines.direction.southeast, args)
end

local function move_southwest(player, args)
  move_in_dir(player, defines.direction.southwest, args)
end

local function move_northwest(player, args)
  move_in_dir(player, defines.direction.northwest, args)
end

local function move_northeast(player, args)
  move_in_dir(player, defines.direction.northeast, args)
end

local function right_in_front_of_player(player, modifier)
    if modifier  == nil then
      modifier = 1
    end

    local posOffset = dir_modifiers[player.character.direction]
    return {
      player.character.position.x+(posOffset[1] * modifier),
      player.character.position.y+(posOffset[2] * modifier)
    }
end

local function box_in_front_of_player(player)
  local posOffset = dir_modifiers[player.character.direction]

  local xOffset = posOffset[1]
  local yOffset = posOffset[2]

  local area = {
    {
      player.character.position.x+(xOffset),
      player.character.position.y+(yOffset)
    }
  }

  for i=1,2 do
    table.insert(area, {
      player.character.position.x+(xOffset + i),
      player.character.position.y+(yOffset + i),
    })

    table.insert(area, {
      player.character.position.x+(xOffset - i),
      player.character.position.y+(yOffset - i),
    })
  end

  return area
end

return {
  box_in_front_of_player   = box_in_front_of_player,
  dir                      = dir,
  dir_modifiers            = dir_modifiers,
  move_east                = move_east,
  move_north               = move_north,
  move_northeast           = move_northeast,
  move_northwest           = move_northwest,
  move_south               = move_south,
  move_southeast           = move_southeast,
  move_southwest           = move_southwest,
  move_west                = move_west,
  right_in_front_of_player = right_in_front_of_player,
  stop                     = stop,
  stop_everything          = stop_everything,
}
