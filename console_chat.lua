local stolen        = require("stolen")
local cheating      = require("cheating")
local direction     = require("direction")
local build         = require("build")
local string_utils  = require("string_utils")
local notifications = require("notifications")

local function coal_time(player, args)
    local amount = args[1]
    if amount == nil then
      amount = 5
    end

    local fuel_slot = defines.inventory.fuel

    for x=-20,20 do
      for y=-20,20 do
        stolen.put(player, {player.position.x-x, player.position.y-y}, "coal", amount, fuel_slot)
      end
    end
end

local function iron_time(player, args)
    local entity = "iron-ore"
    local amount = args[1]
    if amount == nil then
      amount = 5
    end
    local furnace_slot = defines.inventory.furnace_source

    for x=-20,20 do
      for y=-20,20 do
          stolen.put(player, {player.position.x-x, player.position.y-y}, entity, amount, furnace_slot)
      end
    end
end


local function banner(player, args)
  local msg = args[1]
  if msg ~= nil then
    notifications.flying_text(player, msg)
  end
end

local function marker(player, args)
	local tag = {
		position = player.position,
		text = args[1] or "Marker"
	}
	player.force.add_chart_tag("nauvis", tag)
end

-- Every single function in this table
-- NEEDS to take in a player
local chat_commands = {
  ["$dir"]       = direction.dir,
  ["$n"]         = direction.move_north,
  ["$north"]     = direction.move_north,
  ["$s"]         = direction.move_south,
  ["$south"]     = direction.move_south,
  ["$e"]         = direction.move_east,
  ["$east"]      = direction.move_east,
  ["$w"]         = direction.move_west,
  ["$west"]      = direction.move_west,
  ["$ne"]        = direction.move_northeast,
  ["$northeast"] = direction.move_northeast,
  ["$nw"]        = direction.move_northwest,
  ["$northwest"] = direction.move_northwest,
  ["$se"]        = direction.move_southeast,
  ["$southeast"] = direction.move_southeast,
  ["$sw"]        = direction.move_southwest,
  ["$southwest"] = direction.move_southwest,
  ["$stop"]      = direction.stop,
  ["$~"]         = direction.stop_everything,

  -- BASIC BUILDING FUNCTIONS
  ["$build"]     = build.build_entity,
  ["$craft"]     = build.craft_entity,
  ["$destroy"]   = build.destroy,
  ["$mine"]      = build.mine,
  ["$stopmine"]  = build.stopmine,
  ["$take"]      = build.take,
  ["$rotate"]    = build.rotate,

  -- CHEATING
  ["$create"]    = cheating.create,
  ["$biters"]    = cheating.create_biters,
  ["$firemode"]  = cheating.firemode,
  ["$friend"]    = cheating.friend,
  ["$killall"]   = cheating.killall,
  ["$killmode"]  = cheating.killmode,
  ["$launch"]    = cheating.launch,
  ["$oilmode"]   = cheating.oilmode,
  ["$reset"]     = cheating.reset,

  -- Too Specific
  ["$iron"]      = iron_time,
  ["$coal"]      = coal_time,

  -- SPECIAL COMMANDS
	["$banner"]    = banner,
	["$marker"]    = marker
}

local function handle(event)
    local player = game.get_player(2)

    local splitMsg = string_utils.mysplit(event.message, "%s+")
    local cmd = string.gsub(splitMsg[1], "%s+", "")
    local remainder = string.gsub(event.message, cmd, "")
    local args = string_utils.mysplit(remainder, "%s+")
    local chat_func = chat_commands[cmd]

    if chat_func ~= nil then
      if player ~= nil then
        print("Executing Command " .. cmd .. " for " .. player.name)

        return chat_func(player, args)
      end
    end

end

return { handle = handle }
